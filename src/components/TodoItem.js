import React, { Component } from 'react';
import classNames from 'classnames';
import './TodoItem.css'
import checkDone from '../img/check-done.svg'
import check from '../img/check.svg'


class TodoItem extends Component {
    render() {
        const {item, onClicked} = this.props;
        let imgCheck = check;
        if (item.isComplete)
        { 
            imgCheck = checkDone;
        }
        return (
            <div  className= {classNames('TodoItem', {'TodoItem-done' : item.isComplete})}>
                <img  onClick={onClicked} heigth={32} width={32} src={imgCheck}></img>
                <p >{this.props.item.title}</p>
            </div>
        );
    }
}

export default TodoItem