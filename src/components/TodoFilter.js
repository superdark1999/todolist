import React, { Component } from 'react';
import classNames from 'classnames';

import './TodoFilter.css';


class TodoFilter extends Component {
  render () {
    let {todoItems, status, onClickAll, onClickActive, onClickCompleted, onClickClear} = this.props;
    return (
      <div className="TodoFilter">
        <div> {todoItems.length} items</div>
        <div className="status"> 
          <button onClick={onClickAll} className={classNames({active: status==='all'})} >All</button>
          <button onClick={onClickActive} className={classNames({active: status==='active'})} >Active</button>
          <button onClick={onClickCompleted} className={classNames({active: status==='completed'})} >completed</button>
        </div>
        <div className="clear">
          <button onClick={onClickClear}>Clear completed</button>
        </div>
      </div>
    );
  }
}

export default TodoFilter;