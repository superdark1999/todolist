import React, { Component } from 'react';
import './App.css';
import TodoItem from './components/TodoItem'
import TodoFilter from './components/TodoFilter';
import tick from './img/tick.svg'

class App extends Component {
  constructor() {
    super();
    this.state = {
      status: 'all',
      newItem: '',
      todoItems: [
      { title: 'Mua bim bim', isComplete: true},
      { title: 'Đi học', isComplete: true},
      { title: 'Đi chơi', isComplete: false}
    ]};
    this.onKeyUp = this.onKeyUp.bind(this);
    this.onChange = this.onChange.bind(this);   
    this.onClickClear = this.onClickClear.bind(this);
    this.onClickAll = this.onClickAll.bind(this);
    this.onClickActive = this.onClickActive.bind(this);
    this.onClickCompleted = this.onClickCompleted.bind(this);
  }
  onClickStatus(a){
    console.log('sdfds');
  }

  onItemClick(item) {
   return (event) => {
     const isComplete = item.isComplete;
     const {todoItems} = this.state;
     const index = todoItems.indexOf(item);
     this.setState({
       todoItems: [
        ...todoItems.slice(0, index),
        {
          ...item,
          isComplete: !isComplete
        },
        ...todoItems.slice(index+1)
       ]
     })
   }
  }
  onKeyUp(event){
    if (event.keyCode === 13){ // enter = 13
      let item = event.target.value;
      if (!item){
        return;
      }
      item = item.trim();
      if (!item){
        return;
      }
      this.setState({ 
        newItem: '',
        todoItems:
        [{ title: item, isComplete: false},
        ...this.state.todoItems]
      })
    } 
  }
  onChange(event){
    this.setState({
      newItem: event.target.value
    })
  }

  filterItems(){
    const {status, todoItems} = this.state;
    if (status === 'active')
      return todoItems.filter((item)=>{
        return item.isComplete === false;
      })
    if (status === 'completed')
      return todoItems.filter((item)=>{
        return item.isComplete === true;
      })
    return todoItems;
  }

  onClickAll(){
    this.setState({
      status: 'all'
    })
  }
  onClickActive(){
    this.setState({
      status: 'active'
    })
  }
  onClickCompleted(){
    this.setState({
      status: 'completed'
    })
  }
  onClickClear(){
    const {todoItems} = this.state;
    const clearItems = todoItems.filter(function(item, index){
      return item.isComplete === false;
    });
    console.log(clearItems);
    this.setState({
      todoItems: clearItems
    })
  }

  render(){
    const { newItem, status} = this.state;
    const todoItems = this.filterItems();
    return (
      <div className="App">
        <div className="header">
          <img src ={tick} width={32} ></img>
          <input onKeyUp={this.onKeyUp}
            type="text"
            placeholder="add a work"
            value ={newItem}
            onChange={this.onChange}
            />
        </div>
        {
          todoItems.map((item, index) => <TodoItem key={index} item={item} onClicked={this.onItemClick(item)} />)
        }   
        {
          <TodoFilter todoItems={todoItems}
            status={status}
            onClickAll={this.onClickAll}
            onClickActive={this.onClickActive}
            onClickCompleted={this.onClickCompleted}
            onClickClear={this.onClickClear}
          />
        }
      </div>
    );

  
  }
}

export default App;
